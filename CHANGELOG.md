<h1 style="color:#0080FF;">PRICE-PER-UNIT-CALCULATOR RELEASE NOTES</h1>

RELEASE NOTES
======================
### release 1
<ul>
    <li>feat: 1 POC</li>
    <li>feat: 3 Do nothing if calculate clicked for not provided data</li>
    <li>feat: 2 Set default measurement unit to weight and item unit to kg</li>
</ul>