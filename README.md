# Price Per Unit Calculator

## Overview
The **Price Per Unit Calculator** enables users to enter the quantity and price of two different products in various measurement units (e.g., kilograms, pieces). Users can compare which product offers a better unit price by simply clicking the "Calculate" button.

## Functionality
Upon entering the data for each product, and after selecting "Calculate", the app processes the input to determine which product has the more economical per-unit cost. It presents this information in a clear format, showing how much can be saved by choosing the more cost-effective option. For instance, if 1 kg of apples costs 10 PLN in a 1 kg package, and 5 kg of apples costs 45 PLN, the app will indicate that purchasing 5 kg of apples is cheaper by 5 PLN per kilogram.

Additionally, the app visually highlights the product that proves to be more cost-effective, facilitating quick assessment and purchasing decisions.

## Benefits
The **Price Per Unit Calculator** allows users to make more informed purchasing decisions, saving money and time that might otherwise be spent on less advantageous offers. This application is a practical tool for anyone looking to optimize their spending, whether for daily shopping or larger investments.

## Example
![img.png](ui_example.png)
