package pl.eked.android.priceperunitcalculator

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import pl.eked.android.priceperunitcalculator.calculator.Calculator
import pl.eked.android.priceperunitcalculator.calculator.strategy.CalculatorAmountStrategy
import pl.eked.android.priceperunitcalculator.calculator.strategy.CalculatorLengthStrategy
import pl.eked.android.priceperunitcalculator.calculator.strategy.CalculatorWeightStrategy
import pl.eked.android.priceperunitcalculator.calculator.strategy.CommonCalculatorImpl

class CalculatorTest {

    private val calculator = Calculator(setOf(CalculatorAmountStrategy(), CalculatorLengthStrategy(CommonCalculatorImpl()), CalculatorWeightStrategy(CommonCalculatorImpl())))

    @Test
    fun calculate() {
        //given
        val item1 = Item(
            id = 1,
            amount = 100.00,
            unit = "kg",
            price = 100.00.toBigDecimal()
        )

        val item2 = Item(
            id = 2,
            amount = 2.00,
            unit = "t",
            price = 12.00.toBigDecimal()
        )

        val expected = Pair(2, 123.00.toBigDecimal())

        //when
        val result = calculator.calculate(item1, item2, "g")

        //then
        assertThat(result).isEqualTo(expected)
    }
}