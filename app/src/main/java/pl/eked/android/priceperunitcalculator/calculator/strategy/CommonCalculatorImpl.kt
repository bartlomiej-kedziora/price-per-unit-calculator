package pl.eked.android.priceperunitcalculator.calculator.strategy

import pl.eked.android.priceperunitcalculator.Item
import java.math.BigDecimal
import java.math.RoundingMode
import kotlin.math.max

class CommonCalculatorImpl : CommonCalculator {

    override fun calculate(item1: Item, item2: Item, destUnit: String, lowerUnit: String, unitMap: Map<String, Double>) : Pair<Int, BigDecimal> {
        val aligned = alignByUnit(item1, item2, lowerUnit, unitMap)
        val alignedItem1 = aligned.first
        val alignedItem2 = aligned.second

        val diffPrice = alignedItem1.price.subtract(alignedItem2.price).abs()

        val winnerId = if (diffPrice.toDouble() == 0.0)
            0
        else if (alignedItem1.price > alignedItem2.price)
            alignedItem2.id
        else
            alignedItem1.id

        val roundedDiffPrice = getLowerUnit(destUnit, unitMap).toBigDecimal()
            .multiply(diffPrice)
            .setScale(2, RoundingMode.HALF_UP)

        return Pair(winnerId, roundedDiffPrice)
    }

    private fun alignByUnit(item1: Item, item2: Item, unit: String, unitMap: Map<String, Double>) : Pair<Item, Item> {
        val amountPerLowerUnit1 = item1.amount * getLowerUnit(item1.unit, unitMap)
        val amountPerLowerUnit2 = item2.amount * getLowerUnit(item2.unit, unitMap)

        val ratio = getRatio(amountPerLowerUnit1, amountPerLowerUnit2)

        val alignedPrice1 = alignPrice(amountPerLowerUnit1, amountPerLowerUnit2, item1.price, ratio)
        val alignedPrice2 = alignPrice(amountPerLowerUnit2, amountPerLowerUnit1, item2.price, ratio)

        val higherAmount = max(amountPerLowerUnit1, amountPerLowerUnit2).toBigDecimal()

        val pricePerLowerUnit1 =  alignedPrice1.divide(higherAmount, 10, RoundingMode.HALF_UP)
        val pricePerLowerUnit2 = alignedPrice2.divide(higherAmount, 10, RoundingMode.HALF_UP)

        val alignedItem1 = Item(
            id = 1,
            amount = 1.0,
            unit = unit,
            price = pricePerLowerUnit1
        )
        val alignedItem2 = Item(
            id = 2,
            amount = 1.0,
            unit = unit,
            price = pricePerLowerUnit2
        )
        return Pair(alignedItem1, alignedItem2)
    }

    private fun getLowerUnit(unit: String, unitMap: Map<String, Double>) : Double {
        return unitMap[unit] ?: 0.0
    }

    private fun getRatio(amount1: Double, amount2: Double) : Double {
        if (amount1 >= amount2)
            return amount1 / amount2
        return amount2 / amount1
    }

    private fun alignPrice(currentAmount: Double, otherAmount: Double, price: BigDecimal, ratio: Double): BigDecimal {
        if (currentAmount >= otherAmount)
            return price
        return price.multiply(ratio.toBigDecimal())
    }
}