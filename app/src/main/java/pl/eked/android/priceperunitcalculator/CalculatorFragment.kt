package pl.eked.android.priceperunitcalculator

import android.icu.util.Currency
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationBarView.OnItemSelectedListener
import dagger.hilt.android.AndroidEntryPoint
import pl.eked.android.priceperunitcalculator.calculator.utils.MeasurementUnit
import pl.eked.android.priceperunitcalculator.calculator.utils.UnitTool
import pl.eked.android.priceperunitcalculator.calculator.utils.validatePayload
import pl.eked.android.priceperunitcalculator.databinding.FragmentCalculatorBinding
import java.math.BigDecimal

@AndroidEntryPoint
class CalculatorFragment : Fragment(), OnItemSelectedListener {
    private lateinit var lengthKey: String
    private lateinit var weightKey: String
    private lateinit var amountKey: String
    private lateinit var binding: FragmentCalculatorBinding

    companion object {
        private val LENGTH = UnitTool.UNIT_LENGTH_MAP.keys.toList()
        private val WEIGHT = UnitTool.UNIT_WEIGHT_MAP.keys.toList()
        private val AMOUNT = UnitTool.UNIT_AMOUNT_MAP.keys.toList()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        lengthKey = getString(R.string.calculator_fragment_length)
        weightKey = getString(R.string.calculator_fragment_weight)
        amountKey = getString(R.string.calculator_fragment_amount)

        binding = FragmentCalculatorBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setMeasurementUnitSpinner()
        setAmountUnitSpinner()
        setCurrencyType()
    }

    fun getMeasurementUnitType() : MeasurementUnit? {
        return binding.spinnerUnit.selectedItem?.let { UnitTool.translateMeasurementUnit(it.toString(), requireContext()) }
    }

    fun getItems() : List<Item> {
        if (!validatePayload(binding))
            return emptyList()

        return listOf(
            Item(
                id = 1,
                amount = binding.textInputEditAmountItem1.text.toString().toDouble(),
                unit = binding.spinnerItem1.selectedItem.toString(),
                price = binding.textInputEditPriceItem1.text.toString().toDouble().toBigDecimal()
            ),
            Item(
                id = 2,
                amount = binding.textInputEditAmountItem2.text.toString().toDouble(),
                unit = binding.spinnerItem2.selectedItem.toString(),
                price = binding.textInputEditPriceItem2.text.toString().toDouble().toBigDecimal()
            )
        )
    }

    fun setWinner(winnerId: Int, savedAmount: BigDecimal, dstUnit: String) {
        setSummary(winnerId, savedAmount, dstUnit)
        setBorderForWinner(winnerId)
    }

    fun clearData() {
        binding.textInputEditAmountItem1.text?.clear()
//        binding.spinnerItem1.selectedItem,
        binding.textInputEditPriceItem1.text?.clear()

        binding.textInputEditAmountItem2.text?.clear()
//        binding.spinnerItem1.selectedItem,
        binding.textInputEditPriceItem2.text?.clear()

        binding.textViewSummary.text = ""

        binding.containerItem1.background = null
        binding.containerItem2.background = null
    }

    private fun setSummary(winnerId: Int, savedAmount: BigDecimal, dstUnit: String) {
        val summary = when(savedAmount) {
            BigDecimal.ZERO.setScale(2) -> getString(R.string.calculator_fragment_summary_equals_text)
            else -> getString(R.string.calculator_fragment_summary_text,
                winnerId.toString(),
                savedAmount.toPlainString(),
                getCurrencyCode(),
                dstUnit
            )
        }
        binding.textViewSummary.text = summary
    }

    private fun setBorderForWinner(winnerId: Int) {
        val redBorder = AppCompatResources.getDrawable(requireContext(), R.drawable.border_red)
        when(winnerId) {
            1 -> {
                binding.containerItem1.background = redBorder
                binding.containerItem2.background = null
            }
            2 -> {
                binding.containerItem1.background = null
                binding.containerItem2.background = redBorder
            }
        }
    }

    private fun setMeasurementUnitSpinner() {
        val unitSpinnerValues = listOf(lengthKey, weightKey, amountKey)
        val unitSpinnerAdapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, unitSpinnerValues)
        binding.spinnerUnit.adapter = unitSpinnerAdapter
        binding.spinnerUnit.setSelection(1, true)
        binding.spinnerUnit.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                setAmountUnitSpinner()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun setAmountUnitSpinner() {
        val spinnerValues = when (binding.spinnerUnit.selectedItem.toString()) {
            lengthKey -> LENGTH
            weightKey -> WEIGHT
            amountKey -> AMOUNT
            else -> emptyList()
        }
        val spinnerAdapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item, spinnerValues)
        binding.spinnerItem1.adapter = spinnerAdapter
        binding.spinnerItem2.adapter = spinnerAdapter

        setDefaultItemsUnitSelection(spinnerValues)
    }

    /**
     * Default items unit are:
     * - weight -> kilogram
     * - length -> meter
     */
    private fun setDefaultItemsUnitSelection(spinnerValues: List<String>) {
        if (spinnerValues.contains(WEIGHT[1])) {
            val defaultUnitType = UnitTool.getDefaultUnitByMeasuredUnitType(MeasurementUnit.WEIGHT)
            val positionOfDefaultUnitType = WEIGHT.indexOf(defaultUnitType)
            binding.spinnerItem1.setSelection(positionOfDefaultUnitType, true)
            binding.spinnerItem2.setSelection(positionOfDefaultUnitType, true)
        }

        if (spinnerValues.contains(LENGTH[1])) {
            val defaultUnitType = UnitTool.getDefaultUnitByMeasuredUnitType(MeasurementUnit.LENGTH)
            val positionOfDefaultUnitType = LENGTH.indexOf(defaultUnitType)
            binding.spinnerItem1.setSelection(positionOfDefaultUnitType, true)
            binding.spinnerItem2.setSelection(positionOfDefaultUnitType, true)
        }
        clearData()
    }

    private fun setCurrencyType() {
        val currencyCode = getCurrencyCode()

        binding.textViewCurrencyItem1.text = currencyCode
        binding.textViewCurrencyItem2.text = currencyCode
    }

    private fun getCurrencyCode(): String {
        val current = resources.configuration.locales[0]
        val currency = Currency.getInstance(current)
        return currency.currencyCode
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        TODO("Not yet implemented")
    }
}