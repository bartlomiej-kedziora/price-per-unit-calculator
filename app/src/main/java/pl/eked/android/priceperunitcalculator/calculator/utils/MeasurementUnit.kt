package pl.eked.android.priceperunitcalculator.calculator.utils

enum class MeasurementUnit {
    AMOUNT, LENGTH, WEIGHT
}