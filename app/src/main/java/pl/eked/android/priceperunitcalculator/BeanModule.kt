package pl.eked.android.priceperunitcalculator

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoSet
import pl.eked.android.priceperunitcalculator.calculator.Calculator
import pl.eked.android.priceperunitcalculator.calculator.CalculatorStrategy
import pl.eked.android.priceperunitcalculator.calculator.strategy.CalculatorAmountStrategy
import pl.eked.android.priceperunitcalculator.calculator.strategy.CalculatorLengthStrategy
import pl.eked.android.priceperunitcalculator.calculator.strategy.CalculatorWeightStrategy
import pl.eked.android.priceperunitcalculator.calculator.strategy.CommonCalculator
import pl.eked.android.priceperunitcalculator.calculator.strategy.CommonCalculatorImpl

@Module
@InstallIn(SingletonComponent::class)
object BeanModule {

    @Provides
    @IntoSet
    fun provideCalculatorAmountStrategy(): CalculatorStrategy = CalculatorAmountStrategy()

    @Provides
    @IntoSet
    fun provideCalculatorLengthStrategy(commonCalculator: CommonCalculator): CalculatorStrategy = CalculatorLengthStrategy(commonCalculator)

    @Provides
    @IntoSet
    fun provideCalculatorWeightStrategy(commonCalculator: CommonCalculator): CalculatorStrategy = CalculatorWeightStrategy(commonCalculator)

    @Provides
    fun provideCalculator(calculatorStrategies: Set<@JvmSuppressWildcards CalculatorStrategy>) : Calculator = Calculator(calculatorStrategies)

    @Provides
    fun provideCommonCalculator() : CommonCalculator = CommonCalculatorImpl()
}