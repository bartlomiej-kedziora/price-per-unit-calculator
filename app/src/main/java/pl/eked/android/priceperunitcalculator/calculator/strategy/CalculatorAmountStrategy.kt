package pl.eked.android.priceperunitcalculator.calculator.strategy

import pl.eked.android.priceperunitcalculator.Item
import pl.eked.android.priceperunitcalculator.calculator.CalculatorStrategy
import java.math.BigDecimal
import java.math.RoundingMode

class CalculatorAmountStrategy : CalculatorStrategy {
    override fun isApplicable(unit: String): Boolean {
        return unit == "pcs"
    }

    override fun calculate(item1: Item, item2: Item, destUnit: String): Pair<Int, BigDecimal> {
        val item1PricePerOne = item1.price.divide(item1.amount.toBigDecimal(), 2, RoundingMode.HALF_UP)
        val item2PricePerOne = item2.price.divide(item2.amount.toBigDecimal(), 2, RoundingMode.HALF_UP)
        val diffPrice = item1PricePerOne.subtract(item2PricePerOne).abs()

        val winnerId = if (diffPrice.toDouble() == 0.0)
            0
        else if (item1PricePerOne > item2PricePerOne)
            item2.id
        else
            item1.id

        val roundedDiffPrice = diffPrice.setScale(2, RoundingMode.HALF_UP)

        return Pair(winnerId, roundedDiffPrice)
    }
}