package pl.eked.android.priceperunitcalculator.calculator

import pl.eked.android.priceperunitcalculator.Item
import java.math.BigDecimal

interface CalculatorStrategy {

    fun isApplicable(unit: String) : Boolean

    fun calculate(item1: Item, item2: Item, destUnit: String) : Pair<Int, BigDecimal>
}