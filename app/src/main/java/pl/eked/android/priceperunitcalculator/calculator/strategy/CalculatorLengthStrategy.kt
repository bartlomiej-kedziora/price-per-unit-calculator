package pl.eked.android.priceperunitcalculator.calculator.strategy

import pl.eked.android.priceperunitcalculator.Item
import pl.eked.android.priceperunitcalculator.calculator.CalculatorStrategy
import pl.eked.android.priceperunitcalculator.calculator.utils.UnitTool
import java.math.BigDecimal
import javax.inject.Inject

class CalculatorLengthStrategy @Inject constructor(
    private val commonCalculator: CommonCalculator
) : CalculatorStrategy {
    override fun isApplicable(unit: String): Boolean {
        return UnitTool.UNIT_LENGTH_MAP.containsKey(unit)
    }

    override fun calculate(item1: Item, item2: Item, destUnit: String): Pair<Int, BigDecimal> {
        return commonCalculator.calculate(item1, item2, destUnit, "mm", UnitTool.UNIT_LENGTH_MAP)
    }
}