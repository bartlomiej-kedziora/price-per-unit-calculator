package pl.eked.android.priceperunitcalculator.calculator.strategy

import pl.eked.android.priceperunitcalculator.Item
import java.math.BigDecimal

interface CommonCalculator {
    fun calculate(item1: Item, item2: Item, destUnit: String, lowerUnit: String, unitMap: Map<String, Double>) : Pair<Int, BigDecimal>
}