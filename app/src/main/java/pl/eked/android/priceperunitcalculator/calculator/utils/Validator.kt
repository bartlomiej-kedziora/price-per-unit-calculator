package pl.eked.android.priceperunitcalculator.calculator.utils

import android.text.Editable
import pl.eked.android.priceperunitcalculator.databinding.FragmentCalculatorBinding

fun validatePayload(binding: FragmentCalculatorBinding): Boolean {
    return validateItem1Amount(binding)
            && validateItem1Price(binding)
            && validateItem2Amount(binding)
            && validateItem2Price(binding)
}

fun Editable?.isNullOrEmpty() = null == this || this.toString().trim().isEmpty()

private fun validateItem1Amount(binding: FragmentCalculatorBinding): Boolean {
    if (binding.textInputEditAmountItem1.text?.isNullOrEmpty() == true) {
        binding.textInputLayoutAmountItem1.error = "Amount cannot be empty"
        return false
    } else {
        binding.textInputLayoutAmountItem1.error = null
    }
    return true
}

private fun validateItem1Price(binding: FragmentCalculatorBinding): Boolean {
    if (binding.textInputEditPriceItem1.text?.isNullOrEmpty() == true) {
        binding.textInputLayoutPriceItem1.error = "Price cannot be empty"
        return false
    } else {
        binding.textInputLayoutPriceItem1.error = null
    }
    return true
}

private fun validateItem2Amount(binding: FragmentCalculatorBinding): Boolean {
    if (binding.textInputEditAmountItem2.text?.isNullOrEmpty() == true) {
        binding.textInputLayoutAmountItem2.error = "Amount cannot be empty"
        return false
    } else {
        binding.textInputLayoutAmountItem2.error = null
    }
    return true
}

private fun validateItem2Price(binding: FragmentCalculatorBinding): Boolean {
    if (binding.textInputEditPriceItem2.text?.isNullOrEmpty() == true) {
        binding.textInputLayoutPriceItem2.error = "Price cannot be empty"
        return false
    } else {
        binding.textInputLayoutPriceItem2.error = null
    }
    return true
}