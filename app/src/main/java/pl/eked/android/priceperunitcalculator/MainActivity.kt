package pl.eked.android.priceperunitcalculator

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import pl.eked.android.priceperunitcalculator.calculator.Calculator
import pl.eked.android.priceperunitcalculator.calculator.utils.UnitTool
import pl.eked.android.priceperunitcalculator.databinding.ActivityMainBinding
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var calculator: Calculator
    private lateinit var binding: ActivityMainBinding
    private lateinit var calculatorFragment: CalculatorFragment

    @Inject
    fun setCalculator(calculator: Calculator) {
        this.calculator = calculator
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        calculatorFragment  = supportFragmentManager.findFragmentById(binding.frameContent.id) as CalculatorFragment
        setupCalculateButton()
        setupClearButton()
    }

    private fun setupCalculateButton() {
        binding.buttonCalculate.setOnClickListener {
            val destUnit = calculatorFragment.getMeasurementUnitType()
                ?.let {  UnitTool.getDefaultUnitByMeasuredUnitType(it) } ?: throw Exception()
            val items = calculatorFragment.getItems()
            if (items.isNotEmpty()) {
                val calculated = calculator.calculate(item1 = items[0], item2 = items[1], destUnit = destUnit)
                calculatorFragment.setWinner(winnerId = calculated.first, savedAmount = calculated.second, dstUnit = destUnit)
            }
        }
    }

    private fun setupClearButton() {
        binding.buttonClear.setOnClickListener {
            calculatorFragment.clearData()
        }
    }
}