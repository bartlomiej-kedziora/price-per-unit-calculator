package pl.eked.android.priceperunitcalculator.calculator

import pl.eked.android.priceperunitcalculator.Item
import java.math.BigDecimal
import javax.inject.Inject

class Calculator @Inject constructor(
    private val calculatorStrategies: Set<@JvmSuppressWildcards CalculatorStrategy>
) {

    fun calculate(item1: Item, item2: Item, destUnit: String) : Pair<Int, BigDecimal> {
        return calculatorStrategies.first { it.isApplicable(destUnit) }.calculate(item1, item2, destUnit)
    }
}