package pl.eked.android.priceperunitcalculator.calculator.utils

import android.content.Context
import pl.eked.android.priceperunitcalculator.R

class UnitTool {
    companion object {
        @JvmStatic
        val UNIT_AMOUNT_MAP = mapOf(
            "psc" to 1
        )
        @JvmStatic
        val UNIT_LENGTH_MAP = mapOf(
            "nml" to 1852000.0, // nautical mile
            "ml" to 1609344.0, // land mile
            "km" to 1000 * 100 * 10.0, // kilometer
            "m" to 100 * 10.0, // meter
            "yd" to 914.40, // yard
            "ft" to 304.80, // foot
            "dm" to 100.0, // decimeter
            "inch" to 25.40, // inch
            "cm" to 10.0, // centimeter
            "mm" to 1.0, // millimeter
            "um" to 1.0 / 1000, // micrometer(micron)
        )
        @JvmStatic
        val UNIT_WEIGHT_MAP = mapOf(
            "tonUK" to 1016046.90, // ton UK
            "t" to 1000 * 100 * 10.0, // ton
            "tonUS" to 907184.74, // ton US
            "cwtUK" to 50802.35, // hundredweight UK
            "cwt" to 45359.24, // hundredweight US
            "kg" to 100 * 10.0, // kilogram
            "lb" to 453.59, //pound
            "oz" to 28.35, //ounce
            "dag" to 10.0, // decagram
            "g" to 1.0, // gram
            "ct" to 1.0 / 5, // carat
            "gr" to 1 / 15.43 // gran
        )

        @JvmStatic
        val UNIT_DISCRIMINATOR = mapOf(
            MeasurementUnit.AMOUNT to UNIT_AMOUNT_MAP,
            MeasurementUnit.LENGTH to UNIT_LENGTH_MAP,
            MeasurementUnit.WEIGHT to UNIT_WEIGHT_MAP
        )

        fun getMeasuredUnitTypeByUnit(unit: String) : MeasurementUnit {
            return UNIT_DISCRIMINATOR.entries.first{ it.value.containsKey(unit) }.key
        }

        fun getDefaultUnitByMeasuredUnitType(measurementUnit: MeasurementUnit) : String {
            return when(measurementUnit) {
                MeasurementUnit.AMOUNT -> "pcs"
                MeasurementUnit.LENGTH -> "m"
                else -> "kg"
            }
        }

        fun translateMeasurementUnit(measurementUnit: String, context: Context) : MeasurementUnit {
            val amount = context.getString(R.string.calculator_fragment_amount)
            val length = context.getString(R.string.calculator_fragment_length)
            val weight = context.getString(R.string.calculator_fragment_weight)
            val translator=  mapOf(
                MeasurementUnit.AMOUNT to amount,
                MeasurementUnit.LENGTH to length,
                MeasurementUnit.WEIGHT to weight,
            )
            return translator.entries.first { it.value == measurementUnit }.key
        }
    }
}