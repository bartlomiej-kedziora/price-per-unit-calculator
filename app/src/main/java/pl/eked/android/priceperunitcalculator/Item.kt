package pl.eked.android.priceperunitcalculator

import java.io.Serializable
import java.math.BigDecimal

data class Item(
    val id: Int,
    val amount: Double,
    val unit: String,
    val price: BigDecimal
) : Serializable
